import 'dart:async';

import 'package:fluttererrorhandle/Disposable.dart';

import 'errors/errors.dart';

class AppError {
  final message;
  final bool forbidden;

  AppError(this.message, {
    this.forbidden=false
  });
}

mixin ErrorFeedbackMixin implements Disposable {

  final _error = StreamController<AppError>.broadcast();

  Stream<AppError> get errorStream => _error.stream;

  onSuccess() {
    _error.sink.add(null);
  }

  dispose() {
    _error.close();
  }

  onErrorFeedback(error) {
    if (error is ApiError)

      if (error.statusCode == 403)
        _error.sink.add(AppError(error.message,
          forbidden: true
        ));

      else
        _error.sink.add(AppError(error.message));

    else if (error is LogicError)
      _error.sink.add(AppError(error.message));

    else
      throw error;
  }
}
