class LogicError extends Error {

  final String message;

  LogicError(this.message);

  @override
  String toString() => "LogicError: $message";

}