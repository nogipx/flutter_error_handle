
part 'ApiError.g.dart';

class ApiError extends Error {

  ApiError({this.statusCode, this.error, this.message});

  final int statusCode;
  final String error;
  final String message;

  factory ApiError.fromJson(Map<String, dynamic> json) => _$ApiErrorFromJson(json);

  Map<String, dynamic> toJson() => _$ApiErrorToJson(this);

  @override
  String toString() => "ApiError: ($statusCode) $error \nMessage: $message";
}