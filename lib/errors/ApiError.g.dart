// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ApiError.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiError _$ApiErrorFromJson(Map<String, dynamic> json) {
  return ApiError(
    statusCode: json['statusCode'] as int,
    error: json['error'] as String,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$ApiErrorToJson(ApiError instance) => <String, dynamic>{
      'statusCode': instance.statusCode,
      'error': instance.error,
      'message': instance.message,
    };
